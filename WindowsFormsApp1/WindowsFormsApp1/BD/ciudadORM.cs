﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.BD
{
    public static class ciudadORM
    {
        public static List<ciudades> selectCiudades()
        {
            List<ciudades> _ciudades = (
                from c in ORM.bd.ciudades
                orderby c.nombre
                select c
                ).ToList();

            return _ciudades; 
        }
    }
}
