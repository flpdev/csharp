﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.BD
{
    public static class HotelORM
    {
        public static List<hoteles> selectHoteles()
        {

            List<hoteles> _hoteles = (
                from h in ORM.bd.hoteles
                orderby h.nombre
                select h
                ).ToList();

            return _hoteles;
        }

        public static void deleteHotel(hoteles hotel)
        {
            ORM.bd.hoteles.Remove(hotel);
            ORM.bd.SaveChanges();
        }

        public static void updateHotel(hoteles hotel)
        {
            hoteles h = ORM.bd.hoteles.Find(hotel.id_ciudad, hotel.nombre);
            h = hotel;
            ORM.bd.SaveChanges();            
        }

        public static void insertHotel(hoteles hotel)
        {
            ORM.bd.hoteles.Add(hotel);
            ORM.bd.SaveChanges();
        }
    }
}
