﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.BD
{
    public static class cadenaORM
    {
        public static List<cadenas> selectCadenas()
        {
            List<cadenas> _cadenas = (
                from c in ORM.bd.cadenas
                select c).ToList();

            return _cadenas;
        }
    }
}
