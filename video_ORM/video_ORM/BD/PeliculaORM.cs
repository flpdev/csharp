﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace video_ORM.BD
{
    public static class PeliculaORM
    {
        public static List<PELICULAS> selectAllPeliculas()
        {
            List<PELICULAS> _peliculas = (
                from p in ORM.bd.PELICULAS
                orderby p.titulo
                select p
                ).ToList();

            return _peliculas;
        }

        public static List<PELICULAS> selectPeliculaByTitulo(String titulo)
        {
            List<PELICULAS> _peliculas = (
                            from p in ORM.bd.PELICULAS
                            where p.titulo.Contains(titulo)
                            orderby p.titulo
                            select p
                            ).ToList();

            return _peliculas;
        }

        public static void insertPelicula(String titulo, String director, List<TEMAS> temas)
        {
            PELICULAS peli = new PELICULAS();
            peli.titulo = titulo;
            peli.director = director;
            peli.TEMAS = temas;

            ORM.bd.PELICULAS.Add(peli);

            ORM.bd.SaveChanges();
        }

        public static void updatePelicula(int id, String titulo, String director, List<TEMAS> temas)
        {
            PELICULAS peli = ORM.bd.PELICULAS.Find(id);

            peli.titulo = titulo;
            peli.director = director;
            peli.TEMAS = temas;

            ORM.bd.SaveChanges();
        }

        public static void deletePelicula(int id)
        {
            PELICULAS peli = ORM.bd.PELICULAS.Find(id);

            ORM.bd.PELICULAS.Remove(peli);
            ORM.bd.SaveChanges();
        }

        public static void deletePelicula(PELICULAS peli)
        {
            
            ORM.bd.PELICULAS.Remove(peli);
            ORM.bd.SaveChanges();
        }
    }
}
