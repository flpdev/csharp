﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace video_ORM.BD
{
    public static class TemaORM
    {
        public static List<TEMAS> selectAllTemas()
        {
            List<TEMAS> _temas = (
                from p in ORM.bd.TEMAS
                orderby p.descripcion
                select p
                ).ToList();

            return _temas;
        }
    }
}
