﻿using System;
using System.Windows.Forms;
using video_ORM.BD;
namespace video_ORM
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bindingSourcePeliculas.DataSource = PeliculaORM.selectAllPeliculas();
            bindingSourceTemas.DataSource = TemaORM.selectAllTemas();
            

        }

        private void textBoxTitulo_TextChanged(object sender, EventArgs e)
        {
            bindingSourcePeliculas.DataSource = PeliculaORM.selectPeliculaByTitulo(textBoxTitulo.Text);
        }

        private void comboBoxTemas_SelectedIndexChanged(object sender, EventArgs e)
        {
            TEMAS _tema;

            if (comboBoxTemas.SelectedItem != null)
            {
                _tema = (TEMAS)comboBoxTemas.SelectedItem;
                bindingSourcePeliculas.DataSource = _tema.PELICULAS;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FormPeliculas f = new FormPeliculas();

            f.ShowDialog();
        }

        private void dataGridViewPeliculas_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Estas seguro de eliminar la pelicula?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes)
            {
                PeliculaORM.deletePelicula((PELICULAS)dataGridViewPeliculas.SelectedRows[0].DataBoundItem);
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
