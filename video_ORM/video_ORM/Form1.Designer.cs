﻿namespace video_ORM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewPeliculas = new System.Windows.Forms.DataGridView();
            this.idpeliculaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tituloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.directorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourcePeliculas = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxTitulo = new System.Windows.Forms.TextBox();
            this.labelTema = new System.Windows.Forms.Label();
            this.bindingSourceTemas = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxTemas = new System.Windows.Forms.ComboBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPeliculas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePeliculas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTemas)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewPeliculas);
            this.groupBox1.Location = new System.Drawing.Point(26, 146);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(749, 402);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Peliculas";
            // 
            // dataGridViewPeliculas
            // 
            this.dataGridViewPeliculas.AllowUserToAddRows = false;
            this.dataGridViewPeliculas.AutoGenerateColumns = false;
            this.dataGridViewPeliculas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPeliculas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idpeliculaDataGridViewTextBoxColumn,
            this.tituloDataGridViewTextBoxColumn,
            this.directorDataGridViewTextBoxColumn});
            this.dataGridViewPeliculas.DataSource = this.bindingSourcePeliculas;
            this.dataGridViewPeliculas.Location = new System.Drawing.Point(20, 31);
            this.dataGridViewPeliculas.Name = "dataGridViewPeliculas";
            this.dataGridViewPeliculas.ReadOnly = true;
            this.dataGridViewPeliculas.RowTemplate.Height = 24;
            this.dataGridViewPeliculas.Size = new System.Drawing.Size(714, 365);
            this.dataGridViewPeliculas.TabIndex = 0;
            this.dataGridViewPeliculas.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewPeliculas_UserDeletingRow);
            // 
            // idpeliculaDataGridViewTextBoxColumn
            // 
            this.idpeliculaDataGridViewTextBoxColumn.DataPropertyName = "id_pelicula";
            this.idpeliculaDataGridViewTextBoxColumn.HeaderText = "id_pelicula";
            this.idpeliculaDataGridViewTextBoxColumn.Name = "idpeliculaDataGridViewTextBoxColumn";
            this.idpeliculaDataGridViewTextBoxColumn.ReadOnly = true;
            this.idpeliculaDataGridViewTextBoxColumn.Visible = false;
            // 
            // tituloDataGridViewTextBoxColumn
            // 
            this.tituloDataGridViewTextBoxColumn.DataPropertyName = "titulo";
            this.tituloDataGridViewTextBoxColumn.HeaderText = "titulo";
            this.tituloDataGridViewTextBoxColumn.Name = "tituloDataGridViewTextBoxColumn";
            this.tituloDataGridViewTextBoxColumn.ReadOnly = true;
            this.tituloDataGridViewTextBoxColumn.Width = 300;
            // 
            // directorDataGridViewTextBoxColumn
            // 
            this.directorDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.directorDataGridViewTextBoxColumn.DataPropertyName = "director";
            this.directorDataGridViewTextBoxColumn.HeaderText = "director";
            this.directorDataGridViewTextBoxColumn.Name = "directorDataGridViewTextBoxColumn";
            this.directorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bindingSourcePeliculas
            // 
            this.bindingSourcePeliculas.DataSource = typeof(video_ORM.PELICULAS);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Titulo";
            // 
            // textBoxTitulo
            // 
            this.textBoxTitulo.Location = new System.Drawing.Point(92, 51);
            this.textBoxTitulo.Name = "textBoxTitulo";
            this.textBoxTitulo.Size = new System.Drawing.Size(668, 22);
            this.textBoxTitulo.TabIndex = 2;
            this.textBoxTitulo.TextChanged += new System.EventHandler(this.textBoxTitulo_TextChanged);
            // 
            // labelTema
            // 
            this.labelTema.AutoSize = true;
            this.labelTema.Location = new System.Drawing.Point(34, 89);
            this.labelTema.Name = "labelTema";
            this.labelTema.Size = new System.Drawing.Size(44, 17);
            this.labelTema.TabIndex = 3;
            this.labelTema.Text = "Tema";
            // 
            // bindingSourceTemas
            // 
            this.bindingSourceTemas.DataSource = typeof(video_ORM.TEMAS);
            // 
            // comboBoxTemas
            // 
            this.comboBoxTemas.DataSource = this.bindingSourceTemas;
            this.comboBoxTemas.DisplayMember = "descripcion";
            this.comboBoxTemas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTemas.FormattingEnabled = true;
            this.comboBoxTemas.Location = new System.Drawing.Point(92, 86);
            this.comboBoxTemas.Name = "comboBoxTemas";
            this.comboBoxTemas.Size = new System.Drawing.Size(667, 24);
            this.comboBoxTemas.TabIndex = 4;
            this.comboBoxTemas.ValueMember = "id_tema";
            this.comboBoxTemas.SelectedIndexChanged += new System.EventHandler(this.comboBoxTemas_SelectedIndexChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 27);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::video_ORM.Properties.Resources.plus;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 560);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.comboBoxTemas);
            this.Controls.Add(this.labelTema);
            this.Controls.Add(this.textBoxTitulo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPeliculas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePeliculas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTemas)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewPeliculas;
        private System.Windows.Forms.BindingSource bindingSourcePeliculas;
        private System.Windows.Forms.DataGridViewTextBoxColumn idpeliculaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tituloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn directorDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTitulo;
        private System.Windows.Forms.Label labelTema;
        private System.Windows.Forms.BindingSource bindingSourceTemas;
        private System.Windows.Forms.ComboBox comboBoxTemas;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}

