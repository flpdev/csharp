﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using video_ORM.BD;

namespace video_ORM
{
    public partial class FormPeliculas : Form
    {
        public FormPeliculas()
        {
            InitializeComponent();
        }

        private void buttonAceptar_Click(object sender, EventArgs e)
        {

            //List<TEMAS> temas = new List<TEMAS>();

            //foreach(TEMAS tema in listBoxTemas.SelectedItems)
            //{
            //    temas.Add(tema);    
            //}

            List<TEMAS> temas = listBoxTemas.SelectedItems.Cast<TEMAS>().ToList();

            PeliculaORM.insertPelicula(textBoxTitulo.Text, textBoxDirector.Text, temas);
        }

        private void FormPeliculas_Load(object sender, EventArgs e)
        {
            bindingSourceTemas.DataSource = TemaORM.selectAllTemas();
        }
    }
}
