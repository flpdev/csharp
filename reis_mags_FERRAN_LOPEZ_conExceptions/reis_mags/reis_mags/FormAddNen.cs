﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reis_mags
{
    public partial class FormAddNen : Form
    {
        public DataTable tNens { get; set; }
        public FormAddNen()
        {
            InitializeComponent();
        }

       

        private void FormAddNen_Load(object sender, EventArgs e)
        {
            String msg = "";
            comboBox1.DataSource = BD.selectPoblacions(ref msg);
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
            comboBox1.DisplayMember = "nom_p";
            comboBox1.ValueMember = "id_pob";
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAceptar_Click(object sender, EventArgs e)
        {

            Nen nen = new Nen();
            nen.nom = textBoxNom.Text;
            nen.id_pob = (int) comboBox1.SelectedValue;
            nen.data_n = dateTimePicker.Value;


            String msg = BD.insertNen(nen);
            if (msg.Equals(""))
            {
                this.Close();
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
    }
}
