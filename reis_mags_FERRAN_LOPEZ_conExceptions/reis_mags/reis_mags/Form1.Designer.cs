﻿namespace reis_mags
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dadesMestresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nensToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.joguinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.sortirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dadesMestresToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dadesMestresToolStripMenuItem
            // 
            this.dadesMestresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nensToolStripMenuItem,
            this.joguinesToolStripMenuItem,
            this.toolStripMenuItem1,
            this.sortirToolStripMenuItem});
            this.dadesMestresToolStripMenuItem.Name = "dadesMestresToolStripMenuItem";
            this.dadesMestresToolStripMenuItem.Size = new System.Drawing.Size(118, 24);
            this.dadesMestresToolStripMenuItem.Text = "Dades mestres";
            // 
            // nensToolStripMenuItem
            // 
            this.nensToolStripMenuItem.Name = "nensToolStripMenuItem";
            this.nensToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.nensToolStripMenuItem.Text = "Nens";
            this.nensToolStripMenuItem.Click += new System.EventHandler(this.nensToolStripMenuItem_Click);
            // 
            // joguinesToolStripMenuItem
            // 
            this.joguinesToolStripMenuItem.Name = "joguinesToolStripMenuItem";
            this.joguinesToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.joguinesToolStripMenuItem.Text = "Joguines";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(213, 6);
            // 
            // sortirToolStripMenuItem
            // 
            this.sortirToolStripMenuItem.Name = "sortirToolStripMenuItem";
            this.sortirToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.sortirToolStripMenuItem.Text = "Sortir";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dadesMestresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nensToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem joguinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sortirToolStripMenuItem;
    }
}

