﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reis_mags
{
    public partial class FormNens : Form
    {
        public FormNens()
        {
            InitializeComponent();
        }

        private void FormNens_Load(object sender, EventArgs e)
        {
            refreshNens();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FormAddNen fAddNen = new FormAddNen();
            fAddNen.Show();

        }

        private void FormNens_Activated(object sender, EventArgs e)
        {
            refreshNens();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            int i = (int)dataGridViewNens.CurrentRow.Cells[0].Value;

            DialogResult dialog = MessageBox.Show("Segur que vols matar al nen?", "Alert", MessageBoxButtons.YesNo);
            if(dialog== DialogResult.Yes)
            {
                String msg = BD.deleteNen(i);
                if (!msg.Equals(""))
                {
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    refreshNens();
                }
                
            }
            
        }

        public void refreshNens()
        {
            String msg = "";
            dataGridViewNens.DataSource = BD.selectNens(ref msg);
            if (!msg.Equals(""))
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }
    }
}
