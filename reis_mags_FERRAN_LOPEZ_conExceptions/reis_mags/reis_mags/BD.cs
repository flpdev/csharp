﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reis_mags
{
    public static class BD
    {
        public static SqlConnection conexion = new SqlConnection("Data Source=LAPTOP-D57V26TN;Initial Catalog=reis_mags;Integrated Security=SSPI;User ID = sa; Password=cep;");

        public static DataTable selectNens(ref String mensaje)
        {

            DataTable tNens = new DataTable();
            try
            {
                SqlCommand sentencia = new SqlCommand();

                SqlDataReader nens;
                
                sentencia.Connection = conexion;
                sentencia.CommandText = "select * from NENS";

                conexion.Open();
                nens = sentencia.ExecuteReader();
                tNens.Load(nens);
                
            }
            catch (SqlException ex)
            {

                mensaje = mensajeError(ex);
            }
            finally
            {
                conexion.Close();
            }

            return tNens;

        }

        public static DataTable selectPoblacions(ref String msg)
        {
            
            DataTable tPoblacions = new DataTable();
            try
            {
                SqlCommand sentencia = new SqlCommand();

                SqlDataReader poblacions;
                
                sentencia.Connection = conexion;
                sentencia.CommandText = "select * from POBLACIO";

                conexion.Open();
                poblacions = sentencia.ExecuteReader();
                tPoblacions.Load(poblacions);
                

                
            }
            catch (SqlException ex)
            {
                msg = mensajeError(ex);
            }
            finally
            {
                conexion.Close();
            }

            return tPoblacions;

        }

        public static String insertNen(Nen nen)
        {
            String msg = "";
            try
            {
                SqlCommand sentencia = new SqlCommand();

                sentencia.Connection = conexion;

                sentencia.CommandText =
                    "insert into NENS values(@id_pob, @nom, @data_n)";

                sentencia.Parameters.Clear();
                sentencia.Parameters.AddWithValue("@id_pob", nen.id_pob);
                sentencia.Parameters.AddWithValue("@nom", nen.nom);
                sentencia.Parameters.AddWithValue("@data_n", nen.data_n);

                conexion.Open();

                sentencia.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                msg = mensajeError(ex);
                
            }
            finally
            {
                if(conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
            }

            return msg;

        }

        public static String deleteNen(int id_nen)
        {
            String msg = "";
            try
            {
                SqlCommand sentencia = new SqlCommand();

                sentencia.Connection = conexion;

                sentencia.CommandText =
                    "delete from NENS where id_nen = @id_nen";

                sentencia.Parameters.Clear();
                sentencia.Parameters.AddWithValue("@id_nen", id_nen);


                conexion.Open();

                sentencia.ExecuteNonQuery();
            }
            catch(SqlException ex)
            {
                msg = mensajeError(ex);
            }
            finally
            {
                conexion.Close();
            }

            return msg;

        }

        public static String mensajeError(SqlException ex)
        {
            String msg = "";

            switch (ex.Number)
            {
                case -1:
                    msg = "Error de conexión con el servidor";
                    break;

                case 547:
                    msg = "Tiene registros relacionados";
                    break;

                case 2601:
                    msg = "Registro duplicado";
                    break;

                case 2627:
                    msg = "Registro duplicado";
                    break;

                case 4060:
                    msg = "No se encuentra la base de datos";
                    break;

                case 18456:
                    msg = "Usuario o contraseña incorrectos";
                    break;

                default:
                    msg = ex.Number + " - " + ex.Message;
                    break;
            }

            return msg;
        }
    }

    
}
