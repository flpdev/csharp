﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BD;

namespace WindowsFormsApp1
{
    public partial class FormHotel : Form
    {
        public hoteles hotel { get; set; }
        public bool isModification { get; set; }

        public FormHotel()
        {
            InitializeComponent();
        }

        public FormHotel(hoteles hotelModificar)
        {
            this.hotel = hotelModificar;
            this.isModification = true;
            InitializeComponent();
        }

        private void FormHotel_Load(object sender, EventArgs e)
        {
            comboBoxTipos.SelectedIndex = 0;
            bindingSourceCadenas.DataSource = cadenaORM.selectCadenas();
            bindingSourceCiudades.DataSource = ciudadORM.selectCiudades();
        
            if (!(hotel == null))
            {
                this.Text = "Modificar Hotel";
                comboBoxCiudades.SelectedValue = hotel.id_ciudad;
                textBoxNombre.Text = hotel.nombre;
                textBoxCategoria.Text = hotel.categoria.ToString();
                textBoxDireccion.Text = hotel.direccion;
                textBoxTelefono.Text = hotel.telefono.ToString();
                comboBoxTipos.SelectedItem = hotel.tipo;
                comboBoxCadenas.SelectedValue = hotel.cif;
                comboBoxCiudades.Enabled = false;
                textBoxNombre.Enabled = false;
            }

        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAceptar_Click(object sender, EventArgs e)
        {
            int cat;
            Int32.TryParse(textBoxCategoria.Text, out cat);

            if (textBoxNombre.Text.Equals(""))
            {
                MessageBox.Show("El nom no pot estar buit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxNombre.Focus();
            }
            else if(cat < 1 || cat >5)
            {
                MessageBox.Show("La categoria ha de ser entre 1 i 5", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxCategoria.Focus();
            }
            else
            {
                if (isModification)
                {
                    hotel.cadenas = (cadenas)comboBoxCadenas.SelectedItem;
                    hotel.categoria = cat;
                    hotel.direccion = textBoxDireccion.Text;
                    hotel.telefono = Int32.Parse(textBoxTelefono.Text);
                    hotel.tipo = comboBoxTipos.SelectedItem.ToString();
                    hotel.cif = comboBoxCadenas.SelectedValue.ToString();
                    HotelORM.updateHotel(hotel);
                }
                else
                {
                    hotel = new hoteles();
                    hotel.id_ciudad = (int)comboBoxCiudades.SelectedValue;
                    hotel.categoria = cat;
                    hotel.direccion = textBoxDireccion.Text;
                    hotel.telefono = Int32.Parse(textBoxTelefono.Text);
                    hotel.tipo = comboBoxTipos.SelectedItem.ToString();
                    hotel.cif = comboBoxCadenas.SelectedValue.ToString();
                    hotel.nombre = textBoxNombre.Text;
                    HotelORM.insertHotel(hotel);
                }
            }

            this.Close();
        }
    }
}
