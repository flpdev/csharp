﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BD;

namespace WindowsFormsApp1
{
    public partial class FormHoteles : Form
    {
        public FormHoteles()
        {
            InitializeComponent();
        }

        private void FormHoteles_Load(object sender, EventArgs e)
        {
            bindingSourceCiudades.DataSource = ciudadORM.selectCiudades();
            bindingSourceHoteles.DataSource = HotelORM.selectHoteles();
            bindingSourceCadenas.DataSource = cadenaORM.selectCadenas();
        }

        private void comboBoxCiudades_SelectedIndexChanged(object sender, EventArgs e)
        {
            ciudades ciudad;
            if(comboBoxCiudades.SelectedItem != null)
            {
                ciudad = (ciudades)comboBoxCiudades.SelectedItem;
                bindingSourceHoteles.DataSource = ciudad.hoteles.ToList();
               
            }
        }

        private void FormHoteles_Activated(object sender, EventArgs e)
        {
            ciudades ciudad;
            if (comboBoxCiudades.SelectedItem != null)
            {
                ciudad = (ciudades)comboBoxCiudades.SelectedItem;
                bindingSourceHoteles.DataSource = ciudad.hoteles.ToList();

            }
        }

        private void toolStripButtonSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButtonNuevoHotel_Click(object sender, EventArgs e)
        {
            FormHotel fh = new FormHotel();
            fh.ShowDialog();
        }

        private void dataGridViewHoteles_DoubleClick(object sender, EventArgs e)
        {
            FormHotel fh = new FormHotel((hoteles)dataGridViewHoteles.CurrentRow.DataBoundItem);
            fh.ShowDialog();
        }

        private void dataGridViewHoteles_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Estas seguro de eliminar el hotel?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                HotelORM.deleteHotel((hoteles)dataGridViewHoteles.CurrentRow.DataBoundItem);
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
